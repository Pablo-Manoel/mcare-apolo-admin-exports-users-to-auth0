import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mysql.jdbc.Statement;

public class Main {

	public static void main(String[] args) throws Exception {
		JSONArray userList = new JSONArray();
		String query = "SELECT ID, NAME, LOGIN, EMAIL, PASSWORD, TENANTID FROM User where id in (123456944, 123456943)";

		try {
			Connection dbConnection = createDatabaseConnection();
			ResultSet resultQuery = getResultQuery(dbConnection, query);

			while (resultQuery.next()) {
				userList.put(convertToKeyValueFormat(resultQuery));
			}
			System.out.println("lista de usuarios" + userList);
			createFileWithJson(userList);
			
			dbConnection.close();
		} catch (Exception error) {
			System.out.println("ERRO: " + error);
		}
	}

	private static Connection createDatabaseConnection() throws SQLException, Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conexao = DriverManager.getConnection("jdbc:mysql://apolo-wifi-p-nv-db-1.apolowifi.com:3306/apolo",
				"pablo.manoel", "zJVHGkV8JGsT27RY");
		System.out.println("Conectado!");
		return conexao;
	}

	private static ResultSet getResultQuery(Connection connection, String query) throws SQLException {

		Statement st = (Statement) connection.createStatement();
		ResultSet rs = st.executeQuery(query);
		return rs;
	}

	public static JSONObject convertToKeyValueFormat(ResultSet rs) {

		try {
			JSONObject userDetails = new JSONObject();
			userDetails.put("ID", rs.getString("ID"));
			userDetails.put("NAME", rs.getString("NAME"));
			userDetails.put("EMAIL", rs.getString("EMAIL"));
			userDetails.put("LOGIN", rs.getString("LOGIN"));
			userDetails.put("PASSWORD", getDecryptedPassword(rs.getString("PASSWORD")));
			userDetails.put("TENANTID", rs.getString("TENANTID"));

			JSONObject userObject = new JSONObject();
			userObject.put("user", userDetails);
			return userObject;

		} catch (Exception e) {
			System.out.println("[ERRO]: Não foi possivel converter resultado da query em objeto chave-valor ");
			return null;
		}

	}

	private static String getDecryptedPassword(String encryptedPassword) throws Exception {

		CryptoService cryptoService = new CryptoService();
		System.out.println("Senha encriptada: " + encryptedPassword);

		String senha = cryptoService.decrypt(encryptedPassword);
		System.out.println("Senha decriptada: " + senha);
		return senha;

	}

	private static void createFileWithJson(JSONArray userList) throws IOException{
		// Write JSON file
		String tempDirectoryPath = System.getProperty("java.io.tmpdir");
		
		FileWriter file = new FileWriter(tempDirectoryPath + "/USUARIOS-DATABASE.json");

			file.write(userList.toString()); // toJSONString
			file.flush();
			file.close();
			System.out.println("arquivo criado");
	}
}

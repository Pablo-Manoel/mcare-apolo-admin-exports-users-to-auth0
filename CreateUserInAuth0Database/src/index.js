const apoloMysqlService = require('./database-connection/apolo-mobicare-mysql-service');
const ManagementClient = require('auth0').ManagementClient;
const fileSystem = require('fs');
const tempDirectory = require('temp-dir');
const projectRoot = require('app-root-path').path;
const env = require('./environment/variables');

var logUsuariosJaExistentes = [];
var logUsuariosErroAoAdicionar = [];

// const auth0Connection = new ManagementClient({
// 	domain: process.env.DOMAIN,
// 	clientId: process.env.CLIENT_ID,
// 	clientSecret: process.env.CLIENT_SECRET
// });

const auth0Connection = new ManagementClient({
	domain: env.variables.auth0Connection.domain,
	clientId: env.variables.auth0Connection.clientId,
	clientSecret: env.variables.auth0Connection.clientSecret
});

async function importUsersFromApoloToAuth0() {
	let users = await getAllUsersFromTempFile();

	for (let i = 0; i < users.length; i++) {

		let userToBeCreated = getAuth0Payload(users[i].user);

		if (await userExistsInAuth0Database(userToBeCreated)) {
			console.log('Verificação: usuário ja existe na base auth0');
			logUsuariosJaExistentes.push(userToBeCreated);
		} else {
			console.log('Verificação: usuário nao existe na base auth0');
			let user = await addUserInAuth0Database(userToBeCreated);
			await updateExternalIDInApoloDatabase(user);
		}
	}

	await generatelogErrorsIfExists();
	console.log('finished.');
	apoloMysqlService.end();
}

async function generatelogErrorsIfExists() {
	// check if array is empty
	if (logUsuariosJaExistentes.length > 0) {
		console.log('Erros encontrados durante a execucao da aplicação: detalhes em \'logUsuariosJaExistentes.json\'');
		await generateLog(logUsuariosJaExistentes, 'logUsuariosJaExistentes.json');
	}
	if (logUsuariosErroAoAdicionar.length > 0) {
		console.log('Erros encontrados durante a execucao da aplicação: detalhes em \'logUsuariosErroAoAdicionar.json\'');
		await generateLog(logUsuariosErroAoAdicionar, 'logUsuariosErroAoAdicionar.json');
	}
}

async function addUserInAuth0Database(apoloUser) {
	try {
		console.log('Auth0: creating user...');
		return await auth0Connection.createUser(apoloUser);

	} catch (error) {
		logUsuariosErroAoAdicionar.push({ apoloUser, error: error.message });
	}
}

async function generateLog(log, fileName) {
	try {
		// adicionar data/hora da execucao da operacao
		log.push({ "momento da execução": new Date() });
		let res = await fileSystem.writeFile(`${projectRoot}/${fileName}`, JSON.stringify(log));
	} catch (error) {
		console.log('não foi possível criar o arquivo de log com usuarios nao adicionados. Erro:' + error);
	}
}

async function userExistsInAuth0Database(ApoloUser) {
	try {
		user = await auth0Connection.getUser({ id: `auth0|${ApoloUser.user_id}` });
		return true;
	} catch (error) {
		return false;
	}
}

async function getAllUsersFromDatabase() {
	const selectQuery = `SELECT ID, NAME, LOGIN, EMAIL, PASSWORD, TENANTID FROM User where id in (123456944, 123456943)`; // [TODO]: REMOVER WHERE
	try {
		response = await apoloMysqlService.queryAsPromise(selectQuery);

	} catch (error) {
		console.log('Erro ao executar query de obtenção de usuarios: ' + error);
	}
};

async function getAllUsersFromTempFile() {

	try {
		let fileData = fileSystem.readFileSync(`${tempDirectory}/USUARIOS-DATABASE.json`);
		let userList = JSON.parse(fileData);
		return userList;

	} catch (error) {
		console.log('Erro ao resgatar usuarios do arquivo temporario: ' + error);
		return null;
	}
};

function getAuth0Payload(userRow) {
	let auth0Payload = {
		user_id: userRow.ID.toString(), 
		email: userRow.EMAIL,
		password: userRow.PASSWORD,
		name: userRow.NAME,
		blocked: false,
		connection: env.variables.auth0Connection.connection,
		verify_email: false,
		user_metadata: {}
	}

	auth0Payload.user_metadata[env.variables.auth0Connection.instance] = { 
		apolo_user_id: userRow.ID,
		apolo_tenant_id: userRow.TENANTID
	};

	return auth0Payload;

}

async function updateExternalIDInApoloDatabase(user) {

	const selectQuery = `UPDATE apolo.User set EXTERNAL_ID = '${user.user_id}' where id = '${user.identities[0].user_id}'`;
	try {
		response = await apoloMysqlService.queryAsPromise(selectQuery);
	} catch (error) {
		console.log('Erro ao executar query de atualização do external_id: ' + error);
	}
}

//execucao
importUsersFromApoloToAuth0();


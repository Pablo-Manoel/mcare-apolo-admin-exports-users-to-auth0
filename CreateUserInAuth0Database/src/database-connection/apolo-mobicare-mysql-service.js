const mysql = require('mysql');
const env = require('../environment/variables');

const host = () => env.variables.mysqlConnection.host;
const user = () => env.variables.mysqlConnection.user; 
const password = () => env.variables.mysqlConnection.password;
const database = () => env.variables.mysqlConnection.database;
const connectionLimit = () => env.variables.mysqlConnection.connectionLimit;

const pool = mysql.createPool({
  host: host(),
  user: user(),
  password: password(),
  database: database(),
  connectionLimit : connectionLimit(),
});

exports.queryAsPromise = (queryStr) => {
  return new Promise((resolve, reject) => {
    pool.query(queryStr, (error, results, fields) => {
      if (error) {
        reject(error);
      } else {
        resolve({results, fields});
      }
    });
  });
}

exports.end = () => {
  pool.end();
}

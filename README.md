# Objetivo do projeto
- Cadastrar na base do auth0 os usuários existentes na base Apolo OIWIFI

## Funcionamento
- Executar o projeto ExportsUsersAsJSON (JAVA) que exportará o registros dos usuários da base Apolo OIWIFI para um arquivo JSON no diretorio temporário do sistema;
- Executar este projeto que fará a criação do usuário na base do Auth0 e atualização do seu campo external_id na base Apolo;

## Erros
- Se houver erros relacionados à criação do usuário a aplicação criará um arquivo de log contendo o usuário da operação e a causa do erro.
- Demais erros serão notificados no console.